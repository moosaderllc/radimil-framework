#include <iostream>
using namespace std;

#include "NCurses/NcursesApp.hpp"

int main()
{
    NcursesApp app;

    app.DrawCell( 10, 10, 'A', "green-blue" );

    int input = getch();

    return 0;
}
