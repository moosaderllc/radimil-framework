#ifndef _NCURSES_APP_HPP
#define _NCURSES_APP_HPP

#include <ncurses.h>
#include <iostream>
#include <map>
#include <string>
#include <fstream>
using namespace std;

const int MAX_COLORS = 8;

const map<string, short> NC_COLOR_NAME_TO_CODE = {
    { "black", 0 },     { "red", 1 },       { "green", 2 },     { "yellow", 3 },
    { "blue", 4 },      { "magenta", 5 },   { "cyan", 6 },      { "white", 7 }
};

const map<short, string> NC_COLOR_CODE_TO_NAME = {
    { 0, "black" },     { 1, "red" },       { 2, "green" },     { 3, "yellow" },
    { 4, "blue" },      { 5, "magenta" },   { 6, "cyan" },      { 7, "white" }
};

class NcursesApp
{
    public:
    NcursesApp();
    ~NcursesApp();

    void Setup();
    void Teardown();

    void RegisterColorPair( short background, short foreground );
    void RegisterColorPair( const string& colorName );

    void DrawCell( int x, int y, int symbol, const string& colorName );
    void DrawText( int x, int y, char* text );
    void DisplayScreen();

    private:
    map<string,int> m_registeredColorNameToCode;
    int m_totalColors;
    ofstream m_log;

    bool ColorExists( const string& colorName );
};

NcursesApp::NcursesApp()    { Setup(); }
NcursesApp::~NcursesApp()   { Teardown(); }

void NcursesApp::Setup()
{
    m_log << "Setup" << endl;
    initscr();              // Start ncurses
    raw();                  // Disable line buffering
    keypad( stdscr, TRUE ); // F keys
    noecho();               // Don't echo while getch
    start_color();          // Gotta have colors!
    m_totalColors = 0;
    m_log.open( "Log.txt" );
}

void NcursesApp::Teardown()
{
    endwin();               // Byebye ncurses
    m_log.close();
}

void NcursesApp::RegisterColorPair( short background, short foreground )
{
    string colorName =
        NC_COLOR_CODE_TO_NAME[background].second + "-" +
        NC_COLOR_CODE_TO_NAME[foreground].second;

    m_log << "Register color pair \"" << colorName << "\", map to #" << m_totalColors << endl;

    init_pair( m_totalColors, foreground, background );
    m_registeredColorNameToCode[colorName] = m_totalColors;

    m_totalColors++;
}

void NcursesApp::RegisterColorPair( const string& colorName )
{
    size_t posDash = colorName.find( "-" );
    int color1 = NC_COLOR_NAME_TO_CODE[colorName.substr( 0, posDash )];
    int color2 = NC_COLOR_NAME_TO_CODE[colorName.substr( posDash+1, colorName.size()-posDash )];
    RegisterColorPair( color1, color2 );
}

void NcursesApp::DrawCell( int x, int y, int symbol, const string& colorName )
{
    if ( !ColorExists( colorName ) )
    {
        RegisterColorPair( colorName );
    }
    int colorIndex = m_registeredColorNameToCode[colorName];

    attron( COLOR_PAIR( colorIndex ) );
    mvaddch( y, x, symbol );
    attroff( COLOR_PAIR( colorIndex ) );
}

void NcursesApp::DrawText( int x, int y, char* text )
{
    mvprintw( y, x, text );
}

void NcursesApp::DisplayScreen()
{
    refresh();
}

bool NcursesApp::ColorExists( const string& colorName )
{
    map<string, int>::iterator it = m_registeredColorNameToCode.find( colorName );
    return ( it != m_registeredColorNameToCode.end() );
}

#endif
